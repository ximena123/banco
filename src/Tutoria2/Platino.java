/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tutoria2;

/**
 *
 * @author Usuario
 */
public class Platino extends Cuenta {
    public Platino(int numeroCuenta, String nombre) {
        this.numeroCuenta = numeroCuenta;
        this.nombre = nombre;
    }
    @Override
    public double interes() {
        super.interes = super.balance * 0.10;
        return interes;
    }
    @Override
    public void depositar(double valor) {
        super.balance = valor + super.balance;
    }
    @Override
    public double retiro(double valor) {
        super.balance = super.balance - valor;
        return balance;
    }
    public String toString() {
        return "\nCuenta Platino\nSaldo actual de su cuenta es : " + super.balance + " y su interes es: " + interes();
    }
}
