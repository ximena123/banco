/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tutoria2;
import java.util.*;

/**
 *
 * @author Usuario
 */
class Banco {
    static Cheques objCheques = new Cheques(1233, "ximena");
    static Ahorros objAhorros = new Ahorros(1233, "ximena");
    static Platino objPlatino = new Platino(1233, "ximena");
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int menuCuenta;
        System.out.println("Escoge la cuenta\n1.Cheques\n2.Ahorros\n3.Platino\n4.Salir");
        menuCuenta = teclado.nextInt();
        switch (menuCuenta) {
            case 1:
                menu(teclado, menuCuenta);
            case 2:
                menu(teclado, menuCuenta);
                break;
            case 3:
                menu(teclado, menuCuenta);
                break;
        }
    }
    public static void menu(Scanner teclado, int tipo) {
        int c = 0;
        System.out.println("1.Depositar dinero\n2.Retirar dinero\n3.Estado de cuenta\n4.Atras");
        c = teclado.nextInt();
        switch (c) {
            case 1:
                switch (tipo) {
                    case 1:
                        objCheques.depositar(400);
                        System.out.println(objCheques);
                        break;
                    case 2:
                        objAhorros.depositar(400);
                        System.out.println(objAhorros);
                        break;
                    case 3:
                        objPlatino.depositar(400);
                        System.out.println(objPlatino);
                        break;
                    default:
                        break;
                }
                break;
            case 2:
                switch (tipo) {
                    case 1:
                        objCheques.retiro(600);
                        System.out.println(objCheques);
                        break;
                    case 2: {
                        try {
                            objAhorros.retiro(600);
                            System.out.println(objAhorros);
                        } catch (B_Exception ex) {
                            System.err.println("ERROR: "+ex);
                        }
                    }
                    break;
                    case 3:
                        objPlatino.retiro(600);
                        System.out.println(objPlatino);
                        break;
                    default:
                        break;
                }
                break;
            case 3:
                switch (tipo) {
                    case 1:
                        objCheques.interes();
                        System.out.println(objCheques);
                        break;
                    case 2:
                        objAhorros.interes();
                        System.out.println(objAhorros);
                        break;
                    case 3:
                        objPlatino.interes();
                        System.out.println(objPlatino);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
}
