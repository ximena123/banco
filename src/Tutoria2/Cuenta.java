/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tutoria2;

/**
 *
 * @author Usuario
 */
public abstract class Cuenta {
    int numeroCuenta;
    String nombre;
    double interes;
    double balance;
    public abstract double interes();
    public abstract void depositar(double valor);
    public abstract double retiro(double valor) throws B_Exception;
}
