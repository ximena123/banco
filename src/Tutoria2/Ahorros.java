/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tutoria2;

/**
 *
 * @author Usuario
 */
public class Ahorros extends Cuenta {
    public Ahorros(int numeroCuenta, String nombre) {
        this.numeroCuenta = numeroCuenta;
        this.nombre = nombre;
    }
    @Override
    public double interes() {
        super.interes = super.balance * 0.15;
        return interes;
    }
    @Override
    public void depositar(double valor) {
        super.balance = valor + super.balance;
    }
    public double retiro(double valor) throws B_Exception {
        if (valor > super.balance) {
            throw new B_Exception();
        }
        return super.balance - valor;
    }
    public String toString() {
        return "\nCuenta de ahorros\nSaldo actual de su cuenta es : " + super.balance + " y su interes es: " + interes();
    }
}
