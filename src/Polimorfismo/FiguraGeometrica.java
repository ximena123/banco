/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Polimorfismo;
import java.util.*;

public abstract class FiguraGeometrica{
    public double area=2;
    public abstract double area();
}
class Rectangulo extends FiguraGeometrica{
        double base;
       double altura;
    public Rectangulo(double base,double altura) {
        this.altura=altura;
        this.base=base;
    }
    public double area() { 
       area =(base*altura); 
        return area;
    }

    @Override
    public String toString() {
        area();
        return "Area del rectangulo = "+area;
    }
    
}
class Circulo extends FiguraGeometrica{
    double radio ;
    public Circulo(double radio) {
        this.radio = radio;
    }        
    public double area() {
            area = ((Math.PI)*Math.pow(radio, 2));
        return area;
    }

    @Override
    public String toString() {
        area();
        return "El area del circulo = "+area;
    }
    
}
class PruebaFigura{
    public static ArrayList<FiguraGeometrica>Figuras= new  ArrayList<FiguraGeometrica>();
    public static void main(String[] args) {
        FiguraGeometrica Rectangulo= new Rectangulo(2,3);
        FiguraGeometrica Circulo = new Circulo(2);
        Figuras.add(Circulo);
        Figuras.add(Rectangulo);
        for (FiguraGeometrica figura : Figuras) {
             System.out.println(figura);
            
        }
    }
    
    
}