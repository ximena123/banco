/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Deber1;
import java.util.ArrayList;
/**
 *
 * @author Ximena Puchaicela
 */
class Classroom {
    int identifier,maxStudent;
    String subject;
    double porcentageRegister;
    public Classroom(int identifier, int maxStudent, String subject) {
        this.identifier = identifier;
        this.maxStudent = maxStudent;
        this.subject = subject;
    }
    public boolean verifySubject(Teacher objTeacher) throws C_Exception{
        if(this.subject.equals(objTeacher.subject))
            return true;
        else
            throw new C_Exception("Aula asignada a otra materia");
    }
    public double StudentRegister(int studentRegister){
        porcentageRegister =  (studentRegister * 100)/maxStudent;
        return porcentageRegister;
    }
    public boolean verifyStudent() throws C_Exception{
        if(porcentageRegister<50)
            throw new C_Exception("Porcentaje insuficiente para comenzar clases");
        return true;
    }
}
public class Test{
    public static void main(String[] args) {
        int male=0,female=0;
        Student objStudent = new Student();
        ArrayList<Student>people= new  ArrayList<Student>();
        people.add(new Student("Bryan", "Masculino", 15, 10,4,10));
        people.add(new Student("Ximena", "Femenino", 14, 10,6,10));
        try{
            for (Student student : people) {
                objStudent.verifyRank(student);
                objStudent.fail(student,null);
                if (objStudent.verifyQualification(student) && objStudent.verify(objStudent.fail(student,null)));
                System.out.println("Estudiante Apto para recibir clases");
            }
        }catch(C_Exception ex){
            System.err.println(ex);
        }
        Teacher objTeacher = new Teacher("Charlie","Masculino",48,"Filosofia",4,10);
        Classroom objClassroom = new Classroom(48,25,"Filosofia");
        objClassroom.StudentRegister(20);
        try {
            if(objTeacher.verify(objTeacher)&&objClassroom.verifyStudent()&&objClassroom.verifySubject(objTeacher))
                System.out.println("Profesor Apto para brindar clases en el aula "+objClassroom.identifier);
        } catch (C_Exception ex) {
            System.out.println(ex);
        }
        for (Person persons :people) {
             if(persons.sex.equals("Masculino"))
                 male++;
             else
                 female++;  
        }
        System.out.println("Personas de sexo Masculino: "+male+"\nPersonas de sexo Femenino: "+female);
       
    }
}
