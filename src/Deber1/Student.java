/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Deber1;

/**
 *
 * @author Usuario
 */
public class Student extends Person {

    double qualification;

    public Student() {
    }

    public Student(String name, String sex, int age, double qualification, int faults, int numClass) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.qualification = qualification;
        this.faults = faults;
        this.numClass = numClass;
    }
    @Override
    public double fail(Student s, Teacher t) {
        if (t == null) 
            porcentageFaults = (s.faults * 100) / s.numClass;
        return porcentageFaults;
    }

    public boolean verify(double a) throws C_Exception {
        if (a> 50) {
            throw new C_Exception("Numero de faltas excedidas");
        }
        return true;
    }

    public boolean verifyQualification(Student a) throws C_Exception {
        if (a.qualification < 7) {
            throw new C_Exception("Reprobado");
        }
        return true;
    }

    public double verifyRank(Student a) throws C_Exception {
        if (a.qualification > 10) {
            throw new C_Exception("Nota fuera de rango");
        }
        return qualification;
    }

}
