/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Deber1;

/**
 *
 * @author Usuario
 */
public class Teacher extends Person {
    String subject;
    public Teacher(String name, String sex, int age,String subject,int faults,int numClass) {
        this.name= name;
        this.sex= sex;
        this.age= age;
        this.subject= subject;
        this.faults = faults;
        this.numClass= numClass;
    }
    @Override    
    public double fail(Student s, Teacher t) {
        if(s==null)
            porcentageFaults = (t.faults * 100)/ t.numClass;
        return porcentageFaults;
    }
    public boolean verify(Teacher a) throws C_Exception {
        if(a.porcentageFaults > 20){
            throw new C_Exception("Numero de faltas excedidas no puede dictar clases");
        }
        return true; 
    }
}
